from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, PasswordField, BooleanField, SubmitField, SelectField
from wtforms.validators import DataRequired

from briwer_app.persistence.setup_database import setup_connection
from briwer_app.persistence.classes import drink, round, person


import time


class DefaultForm(FlaskForm):
    first_name = StringField("First Name", validators=[
        DataRequired()], description="Enter First Name here")
    surname = StringField("Surname", validators=[
        DataRequired()], description="Enter Surname here")
    submit = SubmitField("Submit", description="Click to submit")


class LoginForm(DefaultForm):
    user_id = IntegerField(
        "User ID (if known)", description="Enter User ID here if known")
    password = PasswordField("Password", validators=[
                             DataRequired()], description="Enter Password here")
    remember_me = BooleanField(
        "Remember Me", description="Click to Remember form data")


class ContactForm(DefaultForm):
    message_box = StringField("Message", validators=[
        DataRequired()], description="Enter message here")


class AddOrderForm(DefaultForm):
    rounds = SelectField("Active Rounds", validators=[
                         DataRequired()], description="Choose an active round")
    drinks = SelectField("Available Drinks", validators=[
                         DataRequired()], description="Choose a drink")


class EndRoundForm(DefaultForm):
    rounds = SelectField("Active Rounds", validators=[
                         DataRequired()], description="Choose an active round")


class AddUserForm(DefaultForm):
    team = StringField("Team", validators=[
        DataRequired()], default="IW Leeds",
        description="Enter team here")
    drink_pref = SelectField(
        "Drink Preference", validators=[
            DataRequired()])

    def list_of_available_drinks(self):
        self.form.drink_pref.choices = drink.get_available_drinks()


class AddDrinkForm(FlaskForm):
    drink_name = StringField("Drink Name (e.g. Americano, Green Tea)", validators=[
                             DataRequired()], description="Enter the drink name here (e.g. Americano, Green Tea)")
    drink_type = StringField("Drink Type (e.g. Coffee, Tea)", validators=[
                             DataRequired()], description="Enter the type of the drink here (e.g. Coffee, Tea)")
    drink_temp = SelectField("Drink Temperature", validators=[DataRequired(
    )], description="Select drink temperature from the two options of hot or cold")
    availability = SelectField("Availability", validators=[DataRequired(
    )], description="Select whether or not the drink is currently available")
    submit = SubmitField("Submit", description="Click to submit")
