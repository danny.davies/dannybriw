from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from briwer_app.flask_api import practice_db


class Person(practice_db.Model):
    id = practice_db.Column(practice_db.Integer, primary_key=True)
    forename = practice_db.Column(
        practice_db.String(64), index=True, unique=True)
    surname = practice_db.Column(
        practice_db.String(64), index=True, unique=True)
    password_hash = practice_db.Column(practice_db.String(128))
    team = practice_db.Column(practice_db.String(64))
    drink_pref_id = practice_db.Column(practice_db.Integer, practice_db.ForeignKey('drink.id'))
    rounds = practice_db.relationship('Round', backref='round_briwer', lazy='dynamic')
    orders = practice_db.relationship('Order', backref='order_member', lazy='dynamic')

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.forename + self.surname)

class Drink(practice_db.Model):
    id = practice_db.Column(practice_db.Integer, primary_key=True)
    drink_name = practice_db.Column(
        practice_db.String(64), index=True, unique=True)
    drink_type = practice_db.Column(
        practice_db.String(64), index=True, unique=True)
    drink_temp = practice_db.Column(
        practice_db.String(4), index=True)
    drink_availabiltiy = practice_db.Column(
        practice_db.Boolean, index=True)
    preference = practice_db.relationship('Person', backref='preferred_by', lazy='dynamic')
    orders = practice_db.relationship('Order', backref='order_item', lazy='dynamic')

    def __repr__(self):
        return '<Drink {}>'.format(self.drink_name)

class Round(practice_db.Model):
    id = practice_db.Column(practice_db.Integer, primary_key=True)
    server_id = practice_db.Column(practice_db.Integer, practice_db.ForeignKey('person.id'))
    active = practice_db.Column(practice_db.Boolean, index=True)    
    timestamp = practice_db.Column(practice_db.DateTime, index=True, default=datetime.utcnow)
    orders = practice_db.relationship('Order', backref='order_creation', lazy='dynamic')

    def __repr__(self):
        return '<Round served by {}>'.format(self.server_id + self.timestamp + self.active)

class Order(practice_db.Model):
    id = practice_db.Column(practice_db.Integer, primary_key=True)
    round_id = practice_db.Column(practice_db.Integer, practice_db.ForeignKey('round.id'))
    person_id = practice_db.Column(practice_db.Integer, practice_db.ForeignKey('person.id'))
    drink_id = practice_db.Column(practice_db.Integer, practice_db.ForeignKey('drink.id'))

    def __repr__(self):
        return '<Order {}>'.format(self.round_id + self.person_id + self.drink_id)
