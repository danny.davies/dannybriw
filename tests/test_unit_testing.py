from briwer_app.functionality import input_cleaner, get_biggest_list_len
import unittest

class test_string_operations(unittest.TestCase):

    def test_input_cleaner(self):
        #Arrange
        input_str = "Bill, 'Ben',  Flower_Pot_Men"
        expected_output = ["Bill", "Ben", "Flower_Pot_Men"]
        #Act
        actual_output = input_cleaner(input_str)
        #Assert
        self.assertEqual(actual_output, expected_output, "Actual output does not equal expected output.")

    def test_get_biggest_list_len(self):
        #Arrange
        list_of_people = ["Danny", "Alan Partridge", "Julio"]
        list_of_drinks = ["Skinny Latte", "Hot Chocolate", "Milk"]
        expected_output_people = 14
        expected_output_drinks = 13
        #Act
        actual_output_people = get_biggest_list_len(list_of_people)
        actual_output_drinks = get_biggest_list_len(list_of_drinks)
        #Assert
        self.assertEqual(actual_output_people, expected_output_people, "Actual output does not equal expected output.")
        self.assertEqual(actual_output_drinks, expected_output_drinks, "Actual output does not equal expected output.")

if __name__ == '__main__':
    unittest.main()