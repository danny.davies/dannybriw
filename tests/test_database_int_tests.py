from briwer_app.persistence.database import save_person_to_database, insert_into_database
from briwer_app.core.classes import Person
import unittest
from unittest.mock import patch
from unittest.mock import call

class TestDatabase (unittest.TestCase):

    def setUp(self):
        self.test_person = Person("Danny","DeVito","team1","Fight Milk")

    @unittest.mock.patch('briwer_app.persistence.database_import.insert_into_database', return_value='')
    def test_that_a_person_is_saved_to_the_database(self, operation):
        #Arrange
        #Act
        save_person_to_database(self.test_person)
        #Assert
        operation.assert_called_once()

if __name__ == "__main__":
    unittest.main()