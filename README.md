# briwer

This version of my briwer application contains the full structure and file system.

# Requirements

    - macOS
    - python3
    - virtualenvwrapper
    - pip / flask / flask-wtf

# Installing Python3

    1) Run 'brew install python3'.

# Cloning/downloading the 'brIWer' application

Visit github site and clone/download all files in the 'brIWer' filesystem. The 'brIWer' app filesystem should be installed in a local file location of your choice.

# Create and use virtual environment to initiate application.

It is advised that you install 'virtualenvwrapper' to create a virtual envnironment in which you can install supplementary software. The virtual environment should be created in the local file location in which the

    1) Run 'pip install virtualenvwrapper' to install virtualenvwrapper (pip is python's package manager).
    2) Navigate to the project folder.
    3) Run 'mkvirtualenv < virtual_env_name >' to creat a virtual environment.
    4) Install further required software inside the virtual environment.

[To exit the virtual environment, run 'deactivate < virtual_env_name >'.][to restart tbe virtual environment, run 'workon < virtual_env_name >'.]

# Installing other required software

Once you have created a virtual environment...

# How to run

This application is intended to be interacted with via a webpage. To run the app, simply follow the below steps:

    1) In your CLI, navigate to the parent directory of your brIWer app.
    2) Run 'python3 -m briwer_app.webpage' to start up a locally hosted server on your machine.
    3) In your browser, navigate to 'localhost:2020/people' to interact with the application.

You can now use the app as and how you wish.

# Setting up a database

The scripts used to set up a local database for use with the application require docker hub to be installed on your machine. Follow the first set of instructions below to download the application and complete set up. If you already have docker hub installed, please skip to the next set of instructions.

    1) Install docker hub via the following macOS container 'https://download.docker.com/mac/stable/Docker.dmg'.
    2) In your Terminal, run 'docker run mysql'. If the MySQL image cannot be found, docker hub will fetch and install the relevant files.
    [NOTE - After the 2nd step above, the following error will likely be seen: "error: database is uninitialized and password option is not specified". This will be fixed by the following set of instructions.]

Once docker hub has been installed, the following instructions should be followed. To change the Name, Password (NEEDS TO BE CHANGED) and Port, simply change the values in the 'initialise_database.sh' file before executing.

    1) In your Terminal, run 'chmod +x <navigate/to/initialise_database.sh>' to make the file executable.
    2) Then run 'initialise_database.sh' to create a local database.
    3) Amend 'test.sql' to create a database with a structure of your choice. The default code will create a three tables in the database for 'People', 'Drinks' and 'Preferences'.
    3) Run '<navigate/to/create_database.sh>' to create the database structure outlined in the 'test.sql' file.
