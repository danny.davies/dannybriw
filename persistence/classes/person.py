import json
from werkzeug.security import generate_password_hash, check_password_hash

from briwer_app.persistence.setup_database import setup_connection, insert_into_db


class Person:

    def __init__(self, forename, surname, team, drink_pref_id):
        self.forename = forename
        self.surname = surname
        self.team = team
        self.drink_pref_id = drink_pref_id

    def to_json(self):
        person_dict = {"forename": self.forename,
                       "surname": self.surname,
                       "team": self.team,
                       "drink_pref_id": self.drink_pref_id}
        return person_dict

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.forename + self.surname)


def get_all_people_from_db():
    db = setup_connection()
    people = {}
    try:
        with db.cursor() as cursor:
            operation = "SELECT * FROM people;"
            cursor.execute(operation)
            results = cursor.fetchall()
            for row in results:
                forename = row[0]
                surname = row[1]
                team = row[2]
                person_id = row[3]
                drink_pref_id = row[4]
                person = Person(forename, surname, team, drink_pref_id)
                people[person_id] = person.to_json()
            return people
    except Exception as e:
        print(e)


def get_list_of_people_dicts():
    list_of_people_dicts = []
    people = get_all_people_from_db()
    for (key, value) in people.items():
        list_of_people_dicts.append(value)
    return list_of_people_dicts


def save_person_to_db(person):
    try:
        sql = f"INSERT INTO people (first_name, surname, team, drink_pref_id) VALUES ('{person.forename}', '{person.surname}', '{person.team}', '{person.drink_pref_id}');"
        insert_into_db(sql)
    except Exception as e:
        print(e)


def check_database_for_person(forename, surname):
    db = setup_connection()
    try:
        with db.cursor() as cursor:
            operation = f"SELECT * FROM people WHERE (first_name = '{forename}' AND surname = '{surname}');"
            cursor.execute(operation)
            person = cursor.fetchone()
            if len(person) == 0:
                error = "This person does not exist."
                return error
            elif len(person) > 0:
                pass
            return person
    except Exception as e:
        print(e)


def get_person_id_from_person(forename, surname):
    person = check_database_for_person(forename, surname)
    person_id = person[3]
    return person_id


def convert_person_id_to_name(person_id):
    db = setup_connection()
    try:
        with db.cursor() as cursor:
            sql = f"SELECT first_name, surname FROM people WHERE person_id = {person_id};"
            cursor.execute(sql)
            person = cursor.fetchone()
            first_name = person[0]
            surname = person[1]
            full_name = first_name + " " + surname
            return full_name
    except Exception as e:
        print(e)
