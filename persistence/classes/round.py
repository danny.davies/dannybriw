import json
import time

from pymysql import cursors

from briwer_app.persistence.setup_database import setup_connection, insert_into_db
from briwer_app.persistence.classes import person, order


class Round:

    def __init__(self, server_id, active):
        self.server_id = server_id
        self.active = active


def create_round(person_id):
    try:
        sql = f"INSERT INTO round (server_id, active) VALUES ('{person_id}', 1);"
        insert_into_db(sql)
    except Exception as e:
        print(e)


def get_new_round_id(person_id):
    db = setup_connection()
    try:
        with db.cursor() as cursor:
            sql = f"SELECT round_id FROM round WHERE server_id = {person_id} AND active = {True};"
            cursor.execute(sql)
            round_id = cursor.fetchone()
            return round_id
    except Exception as e:
        print(e)


def end_round(person_id, round_id):
    db = setup_connection()
    print(f"connection set up before round check {time.time()}", flush=True)
    try:
        with db.cursor() as cursor:
            sql = f"UPDATE round SET active = 0 WHERE server_id = {person_id} AND round_id = {round_id};"
            print(sql, flush=True)
            cursor.execute(sql)
    except Exception as e:
        print(e)


def check_active_rounds():
    active_rounds = []
    db = setup_connection()
    try:
        with db.cursor(cursors.DictCursor) as cursor:
            sql = f"SELECT round_id, CONCAT(p.first_name, ' ', p.surname) as full_name FROM round JOIN people p ON round.server_id = p.person_id WHERE active = 1"
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                round_id = row["round_id"]
                server_name = row["full_name"]
                round_tuple = (round_id, server_name)
                active_rounds.append(round_tuple)
            print(f"loaded active rounds {time.time()}", flush=True)
            return active_rounds
    except Exception as e:
        print(e, flush=True)


def combine_active_rounds_and_orders():
    active_rounds_orders = []
    active_rounds = check_active_rounds()
    for round in active_rounds:
        round_id = round[0]
        server_name = round[1]
        orders = order.retrieve_orders(round_id)
        round_info = "Round " + str(round_id) + " served by " + server_name
        prelim_round_orders = (round_info, orders)
        round_orders = (round_id, prelim_round_orders)
        active_rounds_orders.append(round_orders)
    return active_rounds_orders


def get_active_rounds():
    active_rounds = combine_active_rounds_and_orders()
    active_rounds.insert(0, (0, "--PLEASE CHOOSE AN ACTIVE ROUND--"))
    return active_rounds
