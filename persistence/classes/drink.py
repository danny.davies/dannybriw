import json

from briwer_app.persistence.setup_database import setup_connection, insert_into_db


class Drink:

    def __init__(self, drink_name, drink_type, drink_temp, availability):
        self.name = drink_name
        self.type = drink_type
        self.temp = drink_temp
        self.availability = availability

    def to_json(self):
        drink_dict = {"drink_name": self.name,
                      "drink_type": self.type,
                      "drink_temp": self.temp,
                      "availability": self.availability, }
        return drink_dict


def get_all_drinks_from_db():
    db = setup_connection()
    drinks = {}
    try:
        with db.cursor() as cursor:
            sql = "SELECT * FROM drinks;"
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                drink_id = row[0]
                drink_name = row[1]
                drink_type = row[2]
                availability = row[3]
                drink_temp = row[4]
                drink = Drink(drink_name, drink_type,
                              drink_temp, availability)
                drinks[drink_id] = drink.to_json()
            return drinks
    except Exception as e:
        print(e)


def get_list_of_drink_dicts():
    list_of_drinks_dicts = []
    drinks = get_all_drinks_from_db()
    for (key, value) in drinks.items():
        list_of_drinks_dicts.append(value)
    return list_of_drinks_dicts


def get_available_drinks():
    db = setup_connection()
    list_of_drinks = []
    try:
        with db.cursor() as cursor:
            sql = f"SELECT * FROM drinks WHERE availability = {True};"
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                drink_id = row[0]
                drink_name = row[1]
                drink_tuple = (drink_id, drink_name)
                list_of_drinks.append(drink_tuple)
            return list_of_drinks
    except Exception as e:
        print(e)


def save_drink_to_db(drink):
    try:
        sql = f"INSERT INTO drinks (drink_name, drink_type, temperature, availability) VALUES ('{drink.name}', '{drink.type}', '{drink.temp}', '{drink.availability}');"
        insert_into_db(sql)
    except Exception as e:
        print(e)


def convert_drink_id_to_name(drink_id):
    db = setup_connection()
    try:
        with db.cursor() as cursor:
            sql = f"SELECT * FROM drinks WHERE drink_id = {drink_id};"
            cursor.execute(sql)
            drink = cursor.fetchone()
            drink_name = drink[1]
            return drink_name
    except Exception as e:
        print(e)


def get_drinks():
    list_of_drinks = get_available_drinks()
    list_of_drinks.insert(0, (0, "--PLEASE CHOOSE A DRINK--"))
    return list_of_drinks


def define_drink_availability(avail_choice):
    if avail_choice == "Available":
        availability = 1
    elif avail_choice == "Currently Unavailable":
        availability = 0
    return availability
