import json

from briwer_app.persistence.setup_database import setup_connection, insert_into_db
from briwer_app.persistence.classes import round, person, drink


class Order:

    def __init__(self, round_id, person_id, drink_id):
        self.round_id = round_id
        self.person_id = person_id
        self.drink_id = drink_id


def add_order(forename, surname, round_id, drink_id):
    person_id = person.get_person_id_from_person(forename, surname)
    sql = f"INSERT INTO orders (person_id, drink_id, round_id) VALUES ({person_id}, {drink_id}, {round_id})"
    insert_into_db(sql)


def retrieve_orders(round_id):
    list_of_orders = []
    db = setup_connection()
    try:
        with db.cursor() as cursor:
            sql = f"SELECT * FROM orders WHERE round_id = {round_id};"
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                person_id = row[0]
                drink_id = row[1]
                person_name = person.convert_person_id_to_name(person_id)
                drink_name = drink.convert_drink_id_to_name(drink_id)
                person_order = (person_name, drink_name)
                list_of_orders.append(person_order)
            return list_of_orders
    except Exception as e:
        print(e)
