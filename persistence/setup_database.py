import pymysql
from os import environ


def setup_connection():
    return pymysql.connect(
        environ.get('DB_HOST'),  # host
        environ.get('DB_USER'),  # username
        environ.get('DB_PASSWORD'),  # password
        environ.get('DB_NAME'),  # database
        autocommit=True
    )


def insert_into_db(sql):
    db = setup_connection()
    try:
        with db.cursor() as cursor:
            cursor.execute(sql)
        db.commit()
    finally:
        db.close()
